<?php
return [
    'api'          => env('DEEPL_API'),
    'api_key'      => env('DEEPL_API_KEY'),
    'cache_prefix' => 'DEEPL-',
    'cache_ttl'    => 86400 * 30,
];