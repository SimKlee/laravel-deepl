<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use SimKlee\LaravelDeepl\Console\Commands\ListGlossariesCommand;
use SimKlee\LaravelDeepl\Console\Commands\TranslateCommand;

class LaravelDeeplServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/laravel-deepl.php', 'laravel-deepl');
    }

    public function boot()
    {
        $this->commands([
            ListGlossariesCommand::class,
            TranslateCommand::class,
        ]);

        $this->publishes([
            __DIR__ . '/../config/laravel-deepl.php' => config_path('laravel-deepl.php')
        ], 'config');

        $this->publishes([
            __DIR__ . '/../resources/glossaries.json' => resource_path('json/glossaries.php')
        ], 'resources');

        $this->publishes([
            __DIR__ . '/../database/migrations/deepl_cache.php' =>
                database_path('migrations/2022_11_27_000000_deepl_cache.php'),
        ], 'migrations');
    }
}
