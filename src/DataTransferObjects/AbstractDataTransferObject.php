<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\DataTransferObjects;

use Illuminate\Support\Str;

abstract class AbstractDataTransferObject
{
    abstract public function toArray(): array;

    public static function fromArray(array $data): AbstractDataTransferObject
    {
        $dto = new static();
        collect($data)->each(function (mixed $value, string $key) use ($dto) {
            $dto->{Str::camel($key)} = $value;
        });

        return $dto;
    }

    protected function addToArrayIfNotNull(&$array, $param): void
    {
        if (!is_null($this->{$param})) {
            $array[Str::snake($param)] = $this->boolToString($this->{$param});
        }
    }

    protected function boolToString($value): mixed
    {
        if (is_bool($value)) {
            return $value === true ? '1' : '0';
        }

        return $value;
    }
}