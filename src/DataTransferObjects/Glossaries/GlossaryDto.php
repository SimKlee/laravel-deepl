<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\DataTransferObjects\Glossaries;

use SimKlee\LaravelDeepl\DataTransferObjects\AbstractDataTransferObject;

/**
 * @method GlossaryDto static fromArray(array $data)
 */
class GlossaryDto extends AbstractDataTransferObject
{
    public ?string $glossaryId   = null;
    public ?string $name         = null;
    public ?bool   $ready        = null;
    public ?string $sourceLang   = null;
    public ?string $targetLang   = null;
    public ?string $creationTime = null;
    public ?int    $entryCount   = null;

    public function toArray(): array
    {
        return [
            'glossary_id'   => $this->glossaryId,
            'name'          => $this->name,
            'ready'         => $this->ready,
            'source_lang'   => $this->sourceLang,
            'target_lang'   => $this->targetLang,
            'creation_time' => $this->creationTime,
            'entry_count'   => $this->entryCount,
        ];
    }
}