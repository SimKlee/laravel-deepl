<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\DataTransferObjects\Glossaries;

use SimKlee\LaravelDeepl\DataTransferObjects\AbstractDataTransferObject;

class CreateGlossaryDto extends AbstractDataTransferObject
{
    public string $name;
    public array  $entries;
    public string $sourceLang;
    public string $targetLang;
    public string $entriesFormat = 'tsv';

    public function __construct(string $name, array $entries, string $sourceLang, string $targetLang)
    {
        $this->name       = $name;
        $this->entries    = $entries;
        $this->sourceLang = $sourceLang;
        $this->targetLang = $targetLang;
    }

    private function encodeEntries(): string
    {
        return collect($this->entries)
            ->map(function (string $value, string $key) {
                return sprintf('%s%%09%s', $key, urlencode($value));
            })->implode('%0A');
    }

    public function toArray(): array
    {
        return [
            'name'           => $this->name,
            'source_lang'    => $this->sourceLang,
            'target_lang'    => $this->targetLang,
            'entries'        => $this->encodeEntries(),
            'entries_format' => $this->entriesFormat,
        ];
    }
}