<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\DataTransferObjects;

/**
 * @method TranslateDto static fromArray(array $data)
 */
class TranslateDto extends AbstractDataTransferObject
{
    public const SPLIT_SENTENCES_0          = '0';
    public const SPLIT_SENTENCES_1          = '1';
    public const SPLIT_SENTENCES_NONEWLINES = 'nonewlines';
    public const FORMALITY_DEFAULT          = 'default';
    public const FORMALITY_MORE             = 'more';
    public const FORMALITY_LESS             = 'less';
    public const FORMALITY_PREFER_MORE      = 'prefer_more';
    public const FORMALITY_PREFER_LESS      = 'prefer_less';
    public const TAG_HANDLING_XML           = 'xml';
    public const TAG_HANDLING_HTML          = 'html';

    /**
     * @var string|null Text to be translated. Only UTF-8-encoded plain text is supported.
     */
    public ?string $text = null;

    /**
     * @var string|null Language of the text to be translated.
     */
    public ?string $sourceLang = null;

    /**
     * @var string|null The language into which the text should be translated.
     */
    public ?string $targetLang = null;

    /**
     * @var string Sets whether the translation engine should first split the input into sentences. This is enabled by
     *      default. Possible values are:
     *        0 - no splitting at all, whole input is treated as one sentence
     *        1 (default) - splits on punctuation and on newlines
     *        nonewlines - splits on punctuation only, ignoring newlines
     *      For applications that send one sentence per text parameter, it is advisable to set splitSentences to 0, in
     *      order to prevent the engine from splitting the sentence unintentionally.
     *      Please note that newlines will split sentences. You should therefore clean files to avoid breaking
     *      sentences or set the parameter splitSentences to nonewlines. In the example below, the two parts of the
     *      sentence have been translated separately and this has caused an error: The word "the" has been incorrectly
     *      translated as "die" (the feminine definite article in German), though the German word for "sentence",
     *      "Satz", is masculine (der Satz).
     */
    public string $splitSentences = '1';

    /**
     * @var bool Sets whether the translation engine should respect the original formatting, even if it would usually
     *      correct some aspects.
     *      The formatting aspects affected by this setting include:
     *       - Punctuation at the beginning and end of the sentence
     *       - Upper/lower case at the beginning of the sentence
     */
    public bool $preserveFormatting = false;

    /**
     * @var string Sets whether the translated text should lean towards formal or informal language. This feature
     *      currently only works for target languages DE (German), FR (French), IT (Italian), ES (Spanish), NL (Dutch),
     *      PL (Polish), PT-PT, PT-BR (Portuguese) and RU (Russian). Setting this parameter with a target language that
     *      does not support formality will fail, unless one of the prefer_... options are used.
     *      Possible options are:
     *       - default (default)
     *       - more - for a more formal language
     *       - less - for a more informal language
     *       - prefer_more - for a more formal language if available, otherwise fallback to default formality
     *       - prefer_less - for a more informal language if available, otherwise fallback to default formality
     */
    public string $formality = 'default';

    /**
     * @var string|null Specify the glossary to use for the translation.
     *      Important: This requires the source_lang parameter to be set and the language pair of the glossary has to
     *      match the language pair of the request.
     */
    public ?string $glossaryId = null;

    /**
     * @var string|null Sets which kind of tags should be handled.
     *      Options currently available:
     *       - xml: Enable XML tag handling; see XML Handling.
     *       - html: Enable HTML tag handling; see HTML Handling (Beta).
     */
    public ?string $tagHandling = null;

    /**
     * @var string|null Comma-separated list of XML tags which never split sentences.
     *      For some XML files, finding tags with textual content and splitting sentences using those tags won't yield
     *      the best results.
     */
    public ?string $nonSplittingTags = null;

    /**
     * @var bool The automatic detection of the XML structure won't yield best results in all XML files. You can
     *      disable this automatic mechanism altogether by setting the outline_detection parameter to 0 and selecting
     *      the tags that should be considered structure tags. This will split sentences using the splitting_tags
     *      parameter.
     */
    public bool $outlineDetection = true;

    /**
     * @var string|null Comma-separated list of XML tags which always cause splits.
     */
    public ?string $splittingTags = null;

    /**
     * @var string|null Comma-separated list of XML tags that indicate text not to be translated.
     *      Use this parameter to ensure that elements in the original text are not altered in the translation (e.g.,
     *      trademarks, product names) and insert tags into your original text.
     */
    public ?string $ignoreTags = null;

    public function toArray(): array
    {
        $data = [
            'text'                => $this->text,
            'source_lang'         => $this->sourceLang,
            'target_lang'         => $this->targetLang,
            'split_sentences'     => $this->splitSentences,
            'preserve_formatting' => $this->boolToString($this->preserveFormatting),
            'formality'           => $this->formality,
            'outline_detection'   => $this->boolToString($this->outlineDetection),
        ];

        $this->addToArrayIfNotNull($data, 'glossaryId');
        $this->addToArrayIfNotNull($data, 'tagHandling');
        $this->addToArrayIfNotNull($data, 'nonSplittingTags');
        $this->addToArrayIfNotNull($data, 'splittingTags');
        $this->addToArrayIfNotNull($data, 'ignoreTags');

        return $data;
    }
}