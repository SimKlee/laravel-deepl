<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Exceptions;

use Exception;

class UnknownOptionException extends Exception
{
}