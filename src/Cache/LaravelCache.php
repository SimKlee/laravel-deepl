<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Cache;

use Illuminate\Support\Facades\Cache;
use SimKlee\LaravelDeepl\Responses\AbstractResponse;

class LaravelCache extends AbstractCache
{
    public function ttl(): int
    {
        return config('laravel-deepl.cache_ttl', 86400);
    }

    public function has($key): bool
    {
        if (is_null($key)) {
            return false;
        }

        return Cache::has($key);
    }

    public function get($key): AbstractResponse|false
    {
        if (is_null($key)) {
            return false;
        }

        $response = unserialize(Cache::get($key, false));
        if ($response instanceof AbstractResponse) {
            $response->cached = true;
        }

        return $response;
    }

    public function set($key, AbstractResponse $value): bool
    {
        if (is_null($key)) {
            return false;
        }

        return Cache::put($key, serialize($value), $this->ttl());
    }

    public function delete($key): bool
    {
        if (is_null($key)) {
            return false;
        }

        return Cache::forget($key);
    }

}