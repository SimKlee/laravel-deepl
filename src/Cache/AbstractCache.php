<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Cache;

use SimKlee\LaravelDeepl\Requests\AbstractRequest;

abstract class AbstractCache implements CacheInterface
{
    public function __construct(protected AbstractRequest $request)
    {
    }
}