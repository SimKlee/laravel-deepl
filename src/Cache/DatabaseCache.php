<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Cache;

use SimKlee\LaravelDeepl\Models\DeeplCache;
use SimKlee\LaravelDeepl\Responses\AbstractResponse;

class DatabaseCache extends AbstractCache
{
    public function ttl(): int
    {
        return config('laravel-deepl.cache_ttl', 86400);
    }

    public function has($key): bool
    {
        return DeeplCache::repository()->getLatest($this->ttl()) !== false;
    }

    public function get($key): AbstractResponse|false
    {
        $latest = DeeplCache::repository()->getLatest($key, $this->ttl());
        if ($latest) {
            return unserialize($latest->response);
        }

        return false;
    }

    public function set($key, AbstractResponse $value): bool
    {
        return DeeplCache::repository()->createFromRequest($this->request, $value, $this->ttl()) instanceof DeeplCache;
    }

    public function delete($key): bool
    {
        // TODO: implement delete (all?) cached entries
        return false;
    }
}