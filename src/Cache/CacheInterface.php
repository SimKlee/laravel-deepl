<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Cache;

use SimKlee\LaravelDeepl\Requests\AbstractRequest;
use SimKlee\LaravelDeepl\Responses\AbstractResponse;

interface CacheInterface
{
    public function __construct(AbstractRequest $request);

    public function ttl(): int;

    public function has($key): bool;

    public function get($key): AbstractResponse|false;

    public function set($key, AbstractResponse $value): bool;

    public function delete($key): bool;
}