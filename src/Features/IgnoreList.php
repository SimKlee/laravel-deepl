<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Features;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use SimKlee\LaravelDeepl\DataTransferObjects\AbstractDataTransferObject;
use SimKlee\LaravelDeepl\DataTransferObjects\TranslateDto;
use SimKlee\LaravelDeepl\Responses\AbstractResponse;
use SimKlee\LaravelDeepl\Responses\TranslateResponse;

class IgnoreList extends AbstractFeature
{
    private string $tag = 'ignore';
    private Collection $list;

    public function __construct()
    {
        $this->list = new Collection();
    }

    public function add(array|string $string): void
    {
        if (is_array($string)) {
            collect($string)->each(function (string $item) {
                $this->list->add($item);
            });
        } else {
            $this->list->add($string);
        }
    }

    public function handleDto(AbstractDataTransferObject $dto): void
    {
        match (get_class($dto)) {
            TranslateDto::class => $this->list->each(function (string $item) use ($dto) {
                $dto->text = Str::replace($item, sprintf('<%s>%s</%s>', $this->tag, $item, $this->tag), $dto->text);
            })
        };
    }

    public function handleResponse(AbstractResponse $response): void
    {
        match (get_class($response)) {
            TranslateResponse::class => $response->text = Str::replace(
                [sprintf('<%s>', $this->tag), sprintf('</%s>', $this->tag)], '', $response->text
            )
        };
    }
}