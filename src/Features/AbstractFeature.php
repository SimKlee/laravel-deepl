<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Features;

use SimKlee\LaravelDeepl\DataTransferObjects\AbstractDataTransferObject;
use SimKlee\LaravelDeepl\Responses\AbstractResponse;

abstract class AbstractFeature
{
    abstract public function handleDto(AbstractDataTransferObject $dto): void;

    abstract public function handleResponse(AbstractResponse $response): void;

}