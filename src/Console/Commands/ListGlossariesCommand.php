<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use SimKlee\LaravelDeepl\DataTransferObjects\Glossaries\GlossaryDto;
use SimKlee\LaravelDeepl\Requests\Glossaries\ListGlossariesRequest;

class ListGlossariesCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'deepl:glossaries:list';

    /**
     * @var string
     */
    protected $description = 'List all glossaries and their meta-information.';

    public function handle(): int
    {
        $request  = new ListGlossariesRequest();
        $response = $request->send();

        $this->table([
            'Glossary ID',
            'Name',
            'Ready',
            'Entries',
            'Source Lang',
            'Target Lang',
            'Creation',
        ], $response->glossaries->map(function (GlossaryDto $dto) {
            return [
                $dto->glossaryId,
                $dto->name,
                $dto->ready ? 'yes' : 'no',
                $dto->entryCount,
                $dto->sourceLang,
                $dto->targetLang,
                Carbon::parse($dto->creationTime)->format('Y-m-d H:i'),
            ];
        }));

        return self::SUCCESS;
    }
}