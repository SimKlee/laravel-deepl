<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use SimKlee\LaravelDeepl\DataTransferObjects\TranslateDto;
use SimKlee\LaravelDeepl\Features\IgnoreList;
use SimKlee\LaravelDeepl\Requests\TranslateRequest;

class TranslateCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'deepl:translate {target} {--source=en}';

    /**
     * @var string
     */
    protected $description = 'Translate json language files.';

    public function handle(): int
    {
        $targetLang = $this->argument('target');
        $sourceLang = $this->option('source');

        $sourceFile = lang_path($sourceLang . '.json');
        $targetFile = lang_path($targetLang . '.json');
        if (File::exists($targetFile) === false) {
            File::put($targetFile, json_encode([]));
        }

        $target = json_decode(json: File::get($targetFile), associative: true);
        $source = json_decode(json: File::get($sourceFile), associative: true);

        $ignoreList = new IgnoreList();
        $ignoreList->add(config('translation.ignore'));

        $target = collect($source)->mapWithKeys(function (string $value, string $key) use ($ignoreList, $sourceLang, $targetLang, $source) {
            $translated = $this->translate($value, $targetLang, $sourceLang, $ignoreList);
            $this->info($key);
            $bulletList = [sprintf('%s: %s', Str::upper($sourceLang), $source[$key])];
            $bulletList[] = ($translated === false)
                ? sprintf('%s: %s', Str::upper($targetLang), 'SKIPPED')
                : sprintf('%s: %s', Str::upper($targetLang), $translated);
            $this->components->bulletList($bulletList);

            return [$key => $translated ?? null];
        });

        File::put($targetFile, json_encode($target, JSON_PRETTY_PRINT));

        return self::SUCCESS;
    }

    private function translate(string $text, string $targetLang, string $sourceLang, ?IgnoreList $ignoreList = null): string|false
    {
        $data = [
            'text'       => $text,
            'sourceLang' => $sourceLang,
            'targetLang' => $targetLang,
        ];

        $request             = new TranslateRequest(TranslateDto::fromArray($data));
        $request->ignoreList = $ignoreList;
        $response            = $request->send();
        if ($response->successful()) {
            return $response->text;
        }

        return false;
    }
}