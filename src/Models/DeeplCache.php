<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use SimKlee\LaravelDeepl\Models\Repositories\DeeplCacheRepository;

/**
 * @property int    $id
 * @property string $key
 * @property string $path
 * @property array  $params
 * @property string $response
 * @property array  $features
 * @property Carbon $created_at
 * @property Carbon $expires_at
 */
class DeeplCache extends Model
{
    public const TABLE = 'deepl_cache';

    public const PROPERTY_ID         = 'id';
    public const PROPERTY_KEY        = 'key';
    public const PROPERTY_PATH       = 'path';
    public const PROPERTY_PARAMS     = 'params';
    public const PROPERTY_RESPONSE   = 'response';
    public const PROPERTY_FEATURES   = 'features';
    public const PROPERTY_CREATED_AT = 'created_at';
    public const PROPERTY_EXPIRES_AT = 'expires_at';

    /**
     * @var string
     */
    public $table = self::TABLE;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array|string[]
     */
    protected $fillable = [];

    /**
     * @var array|string[]
     */
    protected $guarded = [
        self::PROPERTY_ID,
    ];

    /**
     * @var array|string[]
     */
    protected $dates = [
        self::PROPERTY_CREATED_AT,
        self::PROPERTY_EXPIRES_AT,
    ];

    /**
     * @var array|string[]
     */
    protected $casts = [
        self::PROPERTY_ID       => 'int',
        self::PROPERTY_PARAMS   => 'array',
        self::PROPERTY_FEATURES => 'array',
    ];

    public static function repository(): DeeplCacheRepository
    {
        return new DeeplCacheRepository();
    }
}
