<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Models\Repositories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use SimKlee\LaravelDeepl\Models\DeeplCache;
use SimKlee\LaravelDeepl\Requests\AbstractRequest;
use SimKlee\LaravelDeepl\Responses\AbstractResponse;

class DeeplCacheRepository
{
    public function createFromRequest(AbstractRequest $request, AbstractResponse $response, int $ttl = null): DeeplCache
    {
        return DeeplCache::create([
            DeeplCache::PROPERTY_KEY        => $request->cacheKey(),
            DeeplCache::PROPERTY_PATH       => $request->path(),
            DeeplCache::PROPERTY_PARAMS     => $request->dto->toArray(),
            DeeplCache::PROPERTY_RESPONSE   => serialize($response),
            DeeplCache::PROPERTY_EXPIRES_AT => is_int($ttl) && $ttl > 0 ? Carbon::now()->addSeconds($ttl) : null,
        ]);
    }

    public function getLatest($key, int $ttl = null): Model|DeeplCache|null
    {
        $query = DeeplCache::query()
                           ->where(DeeplCache::PROPERTY_KEY, $key)
                           ->orderBy(DeeplCache::PROPERTY_CREATED_AT, 'desc')
                           ->limit(1);

        if (is_int($ttl) && $ttl > 0) {
            $query->where(function (Builder $query) use ($ttl) {
                return $query->whereNull(DeeplCache::PROPERTY_EXPIRES_AT)
                             ->orWhere(DeeplCache::PROPERTY_EXPIRES_AT, '>=', Carbon::now()->subSeconds($ttl));
            });
        } elseif ($ttl !== 0) {
            $query->where(function (Builder $query) {
                return $query->whereNull(DeeplCache::PROPERTY_EXPIRES_AT)
                             ->orWhere(DeeplCache::PROPERTY_EXPIRES_AT, '>=', Carbon::now());
            });
        }

        return $query->first();
    }

    /**
     * @return Collection|DeeplCache[]
     */
    public function getAll($key): Collection|null
    {
        return DeeplCache::query()
                         ->where(DeeplCache::PROPERTY_KEY, $key)
                         ->orderBy(DeeplCache::PROPERTY_CREATED_AT, 'desc')
                         ->get();
    }

    public function deleteExpired(): int
    {
        return DeeplCache::query()
                         ->where(DeeplCache::PROPERTY_EXPIRES_AT, '>', Carbon::now())
                         ->delete();
    }

    public function deleteByKey(string $key): int
    {
        return DeeplCache::query()
                         ->where(DeeplCache::PROPERTY_KEY, $key)
                         ->delete();
    }

    /**
     * @return Collection|DeeplCache[]
     */
    public function filterParams(array $params): Collection
    {
        $query = DeeplCache::query()
                           ->orderBy(DeeplCache::PROPERTY_CREATED_AT, 'desc');

        foreach ($params as $key => $value) {
            $query->where(sprintf('params->%s', $key), $value);
        }

        return $query->get();
    }
}