<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Requests;

use Illuminate\Support\Facades\Http;
use SimKlee\LaravelDeepl\Cache\CacheInterface;
use SimKlee\LaravelDeepl\Cache\LaravelCache;
use SimKlee\LaravelDeepl\DataTransferObjects\AbstractDataTransferObject;
use SimKlee\LaravelDeepl\Responses\AbstractResponse;

abstract class AbstractRequest
{
    protected bool           $cacheable = false;
    protected CacheInterface $cache;

    public function __construct(public readonly ?AbstractDataTransferObject $dto = null)
    {
        if ($this->cacheable) {
            $cacheClass  = config('laravel-deepl.cache', LaravelCache::class);
            if (!is_null($cacheClass)) {
                $this->cache = new $cacheClass($this);
            }
        }
    }

    protected function headers(): array
    {
        return [];
    }

    abstract public function path(): string;

    abstract protected function method(): string;

    /**
     * @return string|AbstractResponse
     */
    abstract protected function responseClass(): string;

    protected function params(): array
    {
        return [];
    }

    public function beforeSend(): void
    {
    }

    public function send(bool $useCache = true): AbstractResponse
    {
        $this->beforeSend();

        if ($useCache && ($response = $this->cache->get($this->cacheKey())) !== false) {
            return $response;
        }

        $request = Http::withHeaders(array_merge([
            'Authorization' => 'DeepL-Auth-Key ' . config('laravel-deepl.api_key'),
            'Content-Type'  => 'application/x-www-form-urlencoded',
        ], $this->headers()));

        $response       = match ($this->method()) {
            'get' => $request->get(sprintf('%s%s', config('laravel-deepl.api'), $this->path()), $this->params())
        };
        $responseClass  = $this->responseClass();
        $responseObject = new $responseClass($response);
        $this->afterSend($responseObject);

        if ($this->cacheable) {
            $this->cache->set($this->cacheKey(), $responseObject);
        }

        return $responseObject;
    }

    public function afterSend(AbstractResponse $response): void
    {
    }

    public function cacheKey(): string|null
    {
        return null;
    }
}