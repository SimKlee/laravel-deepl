<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Requests;

use SimKlee\LaravelDeepl\DataTransferObjects\TranslateDto;
use SimKlee\LaravelDeepl\Features\IgnoreList;
use SimKlee\LaravelDeepl\Responses\AbstractResponse;
use SimKlee\LaravelDeepl\Responses\TranslateResponse;

/**
 * @property TranslateDto $dto
 * @method TranslateResponse send()
 */
class TranslateRequest extends AbstractRequest
{
    protected bool     $cacheable  = true;
    public ?IgnoreList $ignoreList = null;

    public function cacheKey(): string|null
    {
        return config('laravel-deepl.cache_prefix', 'DEEPL-') . md5(
                $this->dto->sourceLang . $this->dto->targetLang . $this->dto->text
            );
    }

    public function path(): string
    {
        return '/v2/translate';
    }

    protected function method(): string
    {
        return 'get';
    }

    protected function responseClass(): string
    {
        return TranslateResponse::class;
    }

    protected function params(): array
    {
        return $this->dto->toArray();
    }

    public function beforeSend(): void
    {
        if ($this->ignoreList) {
            $this->dto->ignoreTags  = 'ignore';
            $this->dto->tagHandling = TranslateDto::TAG_HANDLING_XML;
            $this->ignoreList->handleDto($this->dto);
        }
    }

    public function afterSend(AbstractResponse $response): void
    {
        $this->ignoreList?->handleResponse($response);
    }
}