<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Requests\Glossaries;

use SimKlee\LaravelDeepl\DataTransferObjects\Glossaries\CreateGlossaryDto;
use SimKlee\LaravelDeepl\Requests\AbstractRequest;
use SimKlee\LaravelDeepl\Responses\Glossaries\CreateGlossaryResponse;

/**
 * @property CreateGlossaryDto $dto
 * @method CreateGlossaryResponse send()
 */
class CreateGlossaryRequest extends AbstractRequest
{

    public function path(): string
    {
        return '/v2/glossaries';
    }

    protected function method(): string
    {
        return 'post';
    }

    protected function responseClass(): string
    {
        return CreateGlossaryResponse::class;
    }
}