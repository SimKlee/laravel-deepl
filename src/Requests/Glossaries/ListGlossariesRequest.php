<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Requests\Glossaries;

use SimKlee\LaravelDeepl\Requests\AbstractRequest;
use SimKlee\LaravelDeepl\Responses\Glossaries\ListGlossariesResponse;

/**
 * @method ListGlossariesResponse send()
 */
class ListGlossariesRequest extends AbstractRequest
{

    public function path(): string
    {
        return '/v2/glossaries';
    }

    protected function method(): string
    {
        return 'get';
    }

    protected function responseClass(): string
    {
        return ListGlossariesResponse::class;
    }
}