<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Responses\Glossaries;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Collection;
use SimKlee\LaravelDeepl\DataTransferObjects\Glossaries\GlossaryDto;
use SimKlee\LaravelDeepl\Responses\AbstractResponse;

class ListGlossariesResponse extends AbstractResponse
{
    public Collection $glossaries;

    public function __construct(protected Response $response)
    {
        $this->glossaries = new Collection();
        parent::__construct($this->response);
    }

    protected function handleResponse(): void
    {
        $this->glossaries = collect($this->response->json('glossaries'))
            ->map(function (array $glossary) {
                return GlossaryDto::fromArray($glossary);
            });
    }
}