<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Responses\Glossaries;

use SimKlee\LaravelDeepl\Responses\AbstractResponse;

class CreateGlossaryResponse extends AbstractResponse
{
    public ?string $glossaryId   = null;
    public ?string $name         = null;
    public ?bool   $ready        = null;
    public ?string $sourceLang   = null;
    public ?string $targetLang   = null;
    public ?string $creationTime = null;
    public ?int    $entryCount   = null;

    protected function handleResponse(): void
    {
        $this->glossaryId   = $this->response->json('glossary_id');
        $this->name         = $this->response->json('name');
        $this->ready        = $this->response->json('ready');
        $this->sourceLang   = $this->response->json('source_lang');
        $this->targetLang   = $this->response->json('target_lang');
        $this->creationTime = $this->response->json('creation_time');
        $this->entryCount   = $this->response->json('entry_count');
    }
}