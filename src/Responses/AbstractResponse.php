<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Responses;

use Illuminate\Http\Client\Response;

abstract class AbstractResponse
{
    public int    $status;
    public string $reason;
    public bool   $cached = false;

    public function __construct(protected Response $response)
    {
        $this->status = $this->response->status();
        $this->reason = $this->response->reason();

        if ($this->response->successful()) {
            $this->handleResponse();
        }
    }

    public function successful(): bool
    {
        return $this->response->successful();
    }

    abstract protected function handleResponse(): void;
}