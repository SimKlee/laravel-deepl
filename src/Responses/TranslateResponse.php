<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeepl\Responses;

class TranslateResponse extends AbstractResponse
{
    /**
     * @var string|null The language detected in the source text. It reflects the value of the sourceLang parameter,
     *      when specified.
     */
    public ?string $detectedSourceLanguage = null;

    /**
     * @var string|null The translated text.
     */
    public ?string $text = null;

    protected function handleResponse(): void
    {
        $translations = $this->response->json('translations');
        $translation  = current($translations);

        $this->detectedSourceLanguage = $translation['detected_source_language'];
        $this->text                   = $translation['text'];
    }
}