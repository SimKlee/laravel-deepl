<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use SimKlee\LaravelDeepl\Models\DeeplCache;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create(DeeplCache::TABLE, function (Blueprint $table) {
            $table->unsignedInteger(DeeplCache::PROPERTY_ID, true);
            $table->string(DeeplCache::PROPERTY_KEY, 50);
            $table->string(DeeplCache::PROPERTY_PATH, 250);
            $table->json(DeeplCache::PROPERTY_PARAMS)->nullable();
            $table->json(DeeplCache::PROPERTY_FEATURES)->nullable();
            $table->text(DeeplCache::PROPERTY_RESPONSE);
            $table->timestamp(DeeplCache::PROPERTY_CREATED_AT)->useCurrent();
            $table->timestamp(DeeplCache::PROPERTY_EXPIRES_AT)->nullable();

            // indexes
            $table->index([DeeplCache::PROPERTY_KEY], DeeplCache::PROPERTY_KEY);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists(DeeplCache::TABLE);
    }
};
